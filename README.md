# jQuery colorpicker inline

## Overview
Provides a field formatter to render jQuery colorpicker content selections as inline CSS.

## Features
Inline CSS color styles bases on node configuration.

## Requirements
<a href="https://drupal.org/project/jquery_colorpicker">jQuery colorpicker</a>

## Known problems
None to this date.

## Pledges
This module will be actively maintained to keep up with developments in jquery_colorpicker_inline module.

## Credits
Development on this module is sponsored by <a href="http://www.previousnext.com.au" title="PreviousNext">PreviousNext</a>

## Similar projects
<a href="https://drupal.org/project/jquery_colorpicker">jquery_colorpicker</a>
